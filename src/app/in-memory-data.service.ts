import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const books = [
      { id: 0,  name: 'Teach yourself Java in 24 hours' },
      { id: 11, name: 'C# in Depth' },
      { id: 12, name: 'Agile Manifesto' },
      { id: 13, name: 'Linux Bible' },
      { id: 14, name: 'The Shell Book' },
      { id: 15, name: 'Unix Power Tools' },
      { id: 16, name: 'Kotlin Guide' },
      { id: 17, name: 'GPU Power Gems' },
      { id: 18, name: 'Lean Startup' },
      { id: 19, name: 'Revolution in the Valley' },
      { id: 20, name: 'Anuário' }
    ];
    return {books};
  }
}